package com.river.testsheet;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class AddItem extends AppCompatActivity implements View.OnClickListener {

    EditText et_name;
    EditText et_phone;
    EditText et_username;
    EditText et_password;
    Button buttonAddItem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        et_name = findViewById(R.id.et_name);
        et_phone = findViewById(R.id.et_phone);
        et_username = findViewById(R.id.et_username);
        et_password = findViewById(R.id.et_password);

        buttonAddItem = findViewById(R.id.btn_add_item);
        buttonAddItem.setOnClickListener(this);


    }

    //This is the part where data is transafeered from Your Android phone to Sheet by using HTTP Rest API calls

    private void addItemToSheet() {

        final ProgressDialog loading = ProgressDialog.show(this, "Adding Item", "Please wait");

        String url = "https://script.google.com/macros/s/AKfycbwibsiPJOQBZCPSMiJ6h0nUN_AE1hKL2jA3r0XfzVZXCJxOJ_U/exec";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.dismiss();
                        Toast.makeText(AddItem.this, response, Toast.LENGTH_LONG).show();
                        startActivity(new Intent(getApplicationContext(), WebActivity.class));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> parmas = new HashMap<>();

                //here we pass params
                parmas.put("action", "addItem");
                parmas.put("name", et_name.getText().toString().trim());
                parmas.put("phone", et_phone.getText().toString().trim());
                parmas.put("username", et_username.getText().toString().trim());
                parmas.put("password", et_password.getText().toString().trim());
                return parmas;
            }
        };

        int socketTimeOut = 50000;// u can change this .. here it is 50 seconds

        RetryPolicy retryPolicy = new DefaultRetryPolicy(socketTimeOut, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(retryPolicy);

        RequestQueue queue = Volley.newRequestQueue(this);

        queue.add(stringRequest);


    }


    @Override
    public void onClick(View v) {

        if (v == buttonAddItem) {
            if (et_name.getText().toString().trim().isEmpty()) {
                Toast.makeText(getApplicationContext(), "Hãy nhập họ tên", Toast.LENGTH_SHORT).show();
            } else if (et_phone.getText().toString().trim().isEmpty()) {
                Toast.makeText(getApplicationContext(), "Hãy nhập số điện thoại", Toast.LENGTH_SHORT).show();
            } else if (et_username.getText().toString().trim().isEmpty()) {
                Toast.makeText(getApplicationContext(), "Hãy nhập tên đăng nhập", Toast.LENGTH_SHORT).show();
            } else if (et_password.getText().toString().trim().isEmpty()) {
                Toast.makeText(getApplicationContext(), "Hãy nhập mật khẩu", Toast.LENGTH_SHORT).show();
            } else {
                addItemToSheet();

            }
        }
    }


}